ln -s ~/chconfigs/.vimrc ~/.vimrc
ln -s ~/chconfigs/.tmux.conf ~/.tmux.conf
ln -s ~/chconfigs/.bashrc ~/.bashrc
git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
vim +BundleInstall +qall

git config --global user.name "Chris Horsley"
git config --global user.email "chris.horsley@csirtfoundry.com"

sudo aptitude -y install ack-grep
