A simple command for getting tmux and vim set up with plugins and configs.

One line install on a fresh system:

        sudo aptitude -y install git && git clone https://chorsley@bitbucket.org/chorsley/chconfigs.git && cd chconfigs && sh setup.sh