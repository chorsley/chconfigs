syntax on

set nocompatible

set autoindent
set smartindent
set expandtab
set tabstop=4
set shiftwidth=4
set showmatch
set vb t_vb=
set ruler
set incsearch
set noswapfile
set guifont=Inconsolata:h18
set t_Co=256
set term=xterm-256color

let g:airline_theme='murmur'
let g:airline_powerline_fonts=1
let g:airline_left_sep=''
let g:airline_right_sep=''
"let g:airline_section_b="%f"
"let g:airline_section_c=""
"let g:airline_section_x=""
" put filetype in fifth section
let g:airline_section_y="%Y"
"let g:Powerline_symbols = 'fancy'

filetype off
filetype plugin on
filetype plugin indent on

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

set laststatus=2

" let Vundle manage Vundle
"  " required! 
Bundle 'gmarik/vundle'

" non github repos
Bundle 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Bundle 'tpope/vim-fugitive'
Bundle 'scrooloose/syntastic'
Bundle 'Lokaltog/powerline-fonts'
"Bundle 'sontek/rope-vim.git'
Bundle 'davidhalter/jedi-vim'
Bundle 'vim-scripts/pep8.git'
Bundle 'ervandew/supertab'
Bundle 'kien/ctrlp.vim.git'
Bundle 'klen/python-mode.git'
Bundle 'gregsexton/MatchTag.git'

autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

let g:jedi#completions_command = "<M-Space>"
set colorcolumn=80
set pastetoggle=<F1>
set ambiwidth=single
" rewrap code block to 80 chr
map q gq}

" for ctrl-p
set wildignore+=*/venv/*,*/tmp/*,*.so,*.swp,*.zip,*.pyc
let g:ctrlp_max_files=0
" popup menu color
highlight Pmenu ctermbg=238 gui=bold

py << EOF
import os.path
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF

let g:pymode = 1
let g:pymode_rope = 0
let g:pymode_rope_rename_bind = '<C-c>rr'
let g:pymode_rope_organize_imports_bind = '<C-c>ro'
let g:pymode_folding = 0

highlight Pmenu ctermbg=238 gui=bold

"highlight SpellBad term=underline gui=undercurl guisp=#deebfa

"if !exists("autocommands_loaded")
"	  let autocommands_loaded = 1
"	    autocmd BufRead,BufNewFile,FileReadPost *.py source ~/.vim/python
"		endif

		" This beauty remembers where you were the last time you edited the file, and returns to the same position.
"		au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

