export PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;33m\]\w\[\e[m\]\[\e[1;32m\]$(__git_ps1 " (%s)")\n\$\[\e[m\] \[\e[1;37m\]'

git config --global alias.undo-commit 'reset --soft HEAD^'

git config --global credential.helper cache

export PATH=$PATH:$HOME/.local/bin

# cdp django cd's you to the django package path, even in a virtualenv.
cdp () {
    cd "$(python -c "import os.path as _, ${1}; \
            print(_.dirname(_.realpath(${1}.__file__[:-1])))"
        )"
}